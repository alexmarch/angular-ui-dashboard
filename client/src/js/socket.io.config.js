/**
 * Created by mas_bk on 12/8/14.
 */
(function(sio, ng){
  ng.module('socket.io',[])
    .constant('SIO_PORT', 3000)
    .constant('SIO_HOST', 'localhost')
    .run(['SIO_PORT', 'SIO_HOST', '$rootScope', function(SIO_PORT, SIO_HOST, $rootScope) {

      var host = location.protocol + '//' + SIO_HOST + ":" + SIO_PORT;
      window.socket = sio.connect(host);

      socket.on('connection_success', function(){
        console.log("Connection successful!");
      });

      socket.on('refresh_experiments', function(data) {
        $rootScope.$broadcast('refresh_experiments', data);
      });

    }]);

})(io, angular);
