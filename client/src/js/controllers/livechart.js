angular.module('app')
  .controller('LiveChartCtrl', ['$scope', '$rootScope', '$expLoader', function($scope, $rootScope, $expLoader) {

    $expLoader.getAllExperiments().then(function(experiments){
      $scope.dataExperements = $scope.experiments = experiments;
    });

    $rootScope.$on('refresh_experiments', function( event, experiments ) {
      $scope.dataExperements = experiments;
      $scope.$apply();
    });

  }]);
