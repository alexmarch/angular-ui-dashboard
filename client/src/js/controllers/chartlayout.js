/**
 * Created by mas_bk on 12/8/14.
 */
angular.module('app')
  .controller('ChartLayoutCtrl', function($scope) {
    $scope.graph = {
      data: [
      ],
      options: {
        labels: ["x", "A", "B"]
      },
      legend: {
        series: {
          A: {
            label: "Series A"
          },
          B: {
            label: "Series B",
            format: 3
          }
        }
      }
    };
    var base_time = Date.parse("2008/07/01");
    var num = 24 * 0.25 * 365;
    for (var i = 0; i < num; i++) {
      $scope.graph.data.push([ new Date(base_time + i * 3600 * 1000),
        i + 50 * (i % 60),        // line
        i + 50 * (i % 60)
        //i * (num - i) * 4.0 / num  // parabola
      ]);
    }
    //console.log($scope.graph.data);
  });