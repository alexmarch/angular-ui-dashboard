'use strict';
/**
 * $expLoader
 * Service for loading data over server socket.io
 */
angular.module('app')
  .service('$expLoader', ['$q', function( $q ) {
    this.getAllExperiments = function() {
      var deffered = $q.defer();
      if( socket ){
        //Emit server event get all experiments
        socket.emit('get_all_experiments');
        //On server event set_data resolve data
        socket.on('set_data', function( data ){
          deffered.resolve( data );
        });
      }
      return deffered.promise;
    };
  }]);