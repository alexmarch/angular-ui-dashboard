FROM ubuntu:12.04
RUN apt-get update && apt-get -y install gcc \
	wget \
	g++ \
	make \
	build-essential \ 
	python \ 
	python-dev && \
	wget http://nodejs.org/dist/v0.10.33/node-v0.10.33.tar.gz && \
	tar xvzf node-v* && \
	cd node-v* && \
	./configure && \
	make -j 8 && \
	make install && \
	wget http://download.redis.io/releases/redis-2.8.18.tar.gz && \
	tar xvzf redis-* && \
	cd redis-* && \
	make
RUN src/redis-server
