### NODE DASHBOARD APP
***
## Install
```sh
cd node-dashboard
npm install
npm start
```
## Debug
```sh
DEBUG=redis,dashboard npm start 
```
***
#### Project structure
Project root:
```sh
/ node-dashboard
-/ client - frontend
-/ server - backend
-/ package.json - app config
```
***
/ client:
```sh
-/ src
       -/ js
           -/ socket.io.config.js - socket.io logic
          -/ config.router.js - app routing
         -/ controllers
                  -/ livechart.js - chart app controller
```
***
/ server:
```sh
-/ app
   -/ data - demo data
        -/ data.js - data config
  -/ dashboard.js - main app 
 -/ redis.js - redis lib
-/ zmq.js - ZMQ lib
```
***
### App config:
package.json
```sh
"config": {
    "workspace": "development",
//Web server config
    "http": {
      "port": 3000,
      "host": "localhost"
    },
//Redis server config
    "redis": {
      "host": "localhost",
      "port": 6379
    },
//ZMQ server config
    "zmq": {
      "host": "localhost",
      "port": 3030
    },
//Redis notifier config
    "notifier": {
      "logLevel": "DEBUG"
    },
//Options 
    "options": {
      "data": true - fill redis demo data
    }
  },
```