'use strict';
/**
 * Redis lib functions for pu/sub, get/set actions
 */
var redis = require('redis'),
  util = require('util'),
  RedisNotifier = require('redis-notifier'),
  logger = require('debug')('redis'),
  client = redis.createClient(
    process.env.npm_package_config_redis_port,
    process.env.npm_package_config_redis_host
  ),
  events = require('events'),
  Q = require('q');

var Redis = function Redis() {
  var self = this;
  events.EventEmitter.call(this);
  /**
   * Initialize event notifier
   * @type {RedisNotifier}
   */
  this.eventNotifier = new RedisNotifier(redis, {
    redis: {
      host: process.env.npm_package_config_redis_host,
      port: process.env.npm_package_config_redis_port
    },
    expired: true,
    evicted: true,
    logLevel: process.env.npm_package_config_notifier_logLevel
  });
  /**
   * Bind events and listening
   */
  this.eventNotifier.on('message', function(pattern, channelPattern, emittedKey) {
    var channel = this.parseMessageChannel(channelPattern);
    logger.debug("Channel:", channel);
    /**
     * Notify client
     */
    self.emitter.emit('notify', channel, emittedKey);
    switch(channel.key) {
      case 'expired':
        this._handleExpired(emittedKey);
        break;
      case "evicted":
        this._handleEvicted(emittedKey);
        break;
      default:
        logger.debug("Unrecognized Channel Type:" + channel.type);
    }
  });
};
Redis.prototype = {
  /**
   * Get data by key
   * @param key
   * @returns {*}
   */
  get: function (key) {
    var deffered = Q.defer();
    client.get(key, function (err, v) {
      if (err) {
        deffered.reject(err);
      }
      deffered.resolve(v);
    });
    return deffered.promise;
  },
  /**
   * Set data by key
   * @param key
   * @param value
   * @returns {*}
   */
  set: function (key, value) {
    var deffered = Q.defer();
    var strVal = JSON.stringify(value);
    client.set(key, strVal, function (err, obj) {
      if (err) {
        deffered.reject(err);
      }
      deffered.resolve(obj);
    });
    return deffered.promise;
  },
  /**
   * Convert data array to spec object structure
   * @param a
   * @returns {{}}
   */
  converArray: function (a) {
    var data = {}, obj = {}, key = "";

    a.forEach(function (experiment) {
      obj = JSON.parse(experiment);
      key = Object.keys(obj)[0];
      data[key] = obj[key];
    });

    return data;
  },
  /**
   * Get all experiments from redis with special keys expression
   * like: Experiment[A-Z]+
   * @returns {*}
   */
  getAllExperiments: function () {
    var deffered = Q.defer();
    var self = this;
    client.keys('*', function (err, keys) {
      var r = [];
      keys.forEach(function (k) {
        if (/Experiment[A-Z]+/.test(k)) {
          r.push(k);
        }
      });
      client.mget(r, function (err, data) {
        if (err) {
          deffered.reject(err);
        }
        deffered.resolve(self.converArray(data));
      });
    });
    return deffered.promise;
  }
};

Redis.prototype.emitter = new events.EventEmitter(); //inherit EventEmitter supper class

module.exports.Redis = Redis;
