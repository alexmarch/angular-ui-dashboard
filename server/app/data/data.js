/**
 * Created by mas_bk on 12/9/14.
 */
var data_temp = require('./data-demo').data_temp;
var data = {
  'ExperimentA': {
    'metadata': {'title': 'ExperimentA'},
    'ChartLayout': {
      'Page0':{
        'pagetitle' : "Correlation rank",
        'charts':  [
        {
            'title': 'Chart00',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
        },
        {
          'title': 'Chart01',
          'data': data_temp,
          'options': {
            rollPeriod: 14,
            showRoller: true,
            customBars: true,
            title: 'Daily Temperatures in New York vs. San Francisco',
            ylabel: 'Temperature (F)',
            legend: 'always',
            labelsDivStyles: { 'textAlign': 'right' }
          }
        },
        {
          'title': 'Chart02',
          'data': data_temp,
          'options': {
            rollPeriod: 14,
            showRoller: true,
            customBars: true,
            title: 'Daily Temperatures in New York vs. San Francisco',
            ylabel: 'Temperature (F)',
            legend: 'always',
            labelsDivStyles: { 'textAlign': 'right' }
          }
        },
        {
          'title': 'Chart03',
          'data': data_temp,
          'options': {
            rollPeriod: 14,
            showRoller: true,
            customBars: true,
            title: 'Daily Temperatures in New York vs. San Francisco',
            ylabel: 'Temperature (F)',
            legend: 'always',
            labelsDivStyles: { 'textAlign': 'right' }
          }
        }
      ]},
      'Page1':{
        'pagetitle' : "Temperature",
        'charts':  [
          {
            'title': 'Chart00',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart01',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart02',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart03',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          }
        ]}
    }
  },
  'ExperimentB': {
    'metadata': {'title': 'ExperimentB'},
    'ChartLayout': {
      'Page0':{
        'pagetitle' : "Dependence",
        'charts':  [
          {
            'title': 'Chart00',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart01',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart02',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart03',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          }
        ]},
      'Page1':{
        'pagetitle' : "Correlation",
        'charts':  [
          {
            'title': 'Chart00',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart01',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart02',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart03',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          }
        ]}
    }
  },
  'ExperimentC': {
    'metadata': {'title': 'ExperimentC'},
    'ChartLayout': {
      'Page0':{
        'pagetitle' : "Correlation next",
        'charts':  [
          {
            'title': 'Chart00',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart01',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart02',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart03',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          }
        ]},
      'Page1':{
        'pagetitle' : "Correlation",
        'charts':  [
          {
            'title': 'Chart00',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart01',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart02',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          },
          {
            'title': 'Chart03',
            'data': data_temp,
            'options': {
              rollPeriod: 14,
              showRoller: true,
              customBars: true,
              title: 'Daily Temperatures in New York vs. San Francisco',
              ylabel: 'Temperature (F)',
              legend: 'always',
              labelsDivStyles: { 'textAlign': 'right' }
            }
          }
        ]}
    }
  }
};

module.exports = data;