'use strict';

var Redis = require('./redis').Redis,
  redis = new Redis(),
  ZMQ = new require('./zmq').ZMQ,
  zmqClient = new ZMQ(),
  debug = require('debug')('dashboard');

module.exports = function (sio) {
  /**
   * Fill redis demo data
   */
  if (process.env.npm_package_config_options_data === 'true') {
    debug("Fill redis demo data...");
    var data = require('./data/data');
    for (var key in data) {
      if (data.hasOwnProperty(key)) {
        var v = {};
        v[key] = data[key];
        redis.set(key, v);
      }
    }
  }
  /**
   * When client connected to socket.io
   */
  sio.on('connection', function (client) {
    debug("Client connection successful");
    client.emit('connection_success'); //Emit event to client
    /**
     * When client emit event [get_all_experiments]
     * server send all experiments
     */
    client.on('get_all_experiments', function () {
      /**
       * Promise call get all experiments
       */
      redis.getAllExperiments().then(function(experiments){
        client.emit('set_data', experiments);
      });

    });
  });

  /**
   * When received any event from redis client refresh data
   */
  redis.emitter.on('notify', function(channel, emittedKey){
    redis.getAllExperiments().then(function(experiments){
      sio.emit('refresh_experiments', data);
    });
  });
  /**
   * Subscribe on event set
   */
  zmqClient.subscribe('set').on('message', function(topic, msg){
    debug("ZMQ client:", topic, msg);
  });
  //zmqSubscriber.on('set', function (data) {
  //  if (data) {
  //    sio.emit('refresh_experiments', data);
  //  }
  //});

}