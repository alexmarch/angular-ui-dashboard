'use strict';
/**
 * ZMQ lib for communication
 */

var zmq = require('zmq'),
  events = require('events'),
  util = require('util');

function ZMQ () {
  var self = this;

  events.EventEmitter.bind(this);

  //Initialize ZMQ subscriber
  this.sub = zmq.socket('sub');
  this.sub.connect(
    "tcp://" + process.env.npm_package_config_zmq_host +
    ":" + process.env.npm_package_config_zmq_port
  );
  this.sub.on('message', function(topic, msg){
    self.emit('message', topic, msg);
  });

  return this;
};

util.inherits(ZMQ, events.EventEmitter);

/**
 * Subscribe an event
 * @param event type: String
 */
ZMQ.prototype.subscribe = function(event){
  this.sub.subscribe(event);
  return this;
};

module.exports.ZMQ = ZMQ;
