'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var logger = require('debug')('server');
var app = module.exports = loopback();

//Config static files folders
var workspace = {
  development: '../client/src',
  production: '../client/dist'
};

/**
 * Config static files folder
 */
var staticFilesPath = require('path').resolve(__dirname,
  process.env.npm_package_config_workspace === "production" ? workspace.production : workspace.development);

var httpServer = require('http').Server(app);
/**
 * Config socket.io / dashboard
 */
var sio = require('socket.io')(httpServer);
var sioLogic = require('.//app/dashboard')(sio); //Config dashboard with socket.io

// Set up the /favicon.ico
app.use(loopback.favicon());

// request pre-processing middleware
app.use(loopback.compress());

// -- Add your pre-processing middleware here --

// boot scripts mount components like REST API
boot(app, __dirname);

// -- Mount static files here--
// All static middleware should be registered at the end, as all requests
// passing the static middleware are hitting the file system
// Example:
//   var path = require('path');
//   app.use(loopback.static(path.resolve(__dirname, '../client')));

// Requests that get this far won't be handled
// by any middleware. Convert them into a 404 error
// that will be handled later down the chain.

/**
 * Set server prop for static files
 */
app.set('clientApp', staticFilesPath);

app.use(loopback.static(app.get('clientApp')));

app.use(loopback.urlNotFound());

app.use(function (err, req, res, next) {
  if (err.status === 404) {
    return res.redirect('/#/access/404'); //if page not found redirect to angular route
  }
  next();
});

// The ultimate error handler.
app.use(loopback.errorHandler());

app.start = function () {
  /**
   * Run server application
   */
  return httpServer.listen(
    process.env.npm_package_config_http_port,
    function () {
      logger('[%s] App start at address: (%s:%d)',
        new Date().toLocaleTimeString(),
        process.env.npm_package_config_http_host,
        process.env.npm_package_config_http_port
      );
    });
};

// start the server if `$ node server.js`
if (require.main === module) {
  app.start();
}
